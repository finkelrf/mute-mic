#!/usr/bin/python3
import os
import yaml
import sys
import threading
import gi
gi.require_version("Gtk", "3.0")
gi.require_version('AppIndicator3', '0.1')
from gi.repository import Gtk as gtk, AppIndicator3 as appindicator
from gi.repository import Gdk
import time
import subprocess
from pynput import keyboard
import threading
from datetime import datetime, timedelta

class ConfigHotkeyWindow(gtk.Window):
    def __init__(self, hotkey):
        gtk.Window.__init__(self, title="Configure Hotkey")
        self.set_size_request(200, 30)
        vbox = gtk.Box(orientation=gtk.Orientation.VERTICAL, spacing=6)
        self.add(vbox)
        self.entry = gtk.Entry()
        vbox.pack_start(self.entry, True, True, 0)
        self.connect("key-press-event", self.on_key_press_event)
        self.connect("key-release-event", self.on_key_release_event)
        self.hotkey = hotkey
        self.hotkey_candidate = []
        self.last_press = datetime.now()
        self.entry.set_text('+'.join([Gdk.keyval_name(keyval) for keyval in self.hotkey.combination[0]]))

    def start(self):
        self.show()
        gtk.main()

    def on_key_press_event(self, widget, event):
        if Gdk.keyval_name(event.keyval) == "Return":
            if self.hotkey.is_valid(self.hotkey_candidate):
                self.hotkey.save_new_combination(self.hotkey_candidate)
                self.hide()
        else:
            if datetime.now() - self.last_press > timedelta(microseconds=300000):
                self.hotkey_candidate = []

            self.hotkey_candidate.append(event.keyval)
            self.last_press = datetime.now()


    def on_key_release_event(self, widget, event):
        self.entry.set_text('+'.join([Gdk.keyval_name(keyval) for keyval in self.hotkey_candidate]))


class SystemTrayIcon():
    def __init__(self, hotkey):
        self.mic_icon, self.muted_mic_icon = self._get_icons()
        self.hotkey = hotkey
        icon = self.mic_icon
        self.muted = False
        if self.is_mic_muted():
            icon = self.muted_mic_icon
            self.muted = True

        self.indicator = appindicator.Indicator.new('custom-tray', icon, appindicator.IndicatorCategory.SYSTEM_SERVICES)
        self.indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
        self.indicator.set_menu(self.menu())
        self.thread = threading.Thread(target=self.mic_state_thread)
        self.thread.start()

    def _get_icons(self):
        icons_list = [
            ("microphone-sensitivity-high-symbolic", "microphone-sensitivity-muted-symbolic"),
            ("microphone-sensitivity-high", "microphone-sensitivity-muted")
        ]
        for icons in icons_list:
            if icons[0] and icons[1] in gtk.IconTheme.list_icons(gtk.IconTheme.get_default()):
                return icons
        print('Could not find icons on the default theme', file=sys.stderr)
        exit(1)

    def start(self):
        gtk.main()

    def is_mic_muted(self):
        cmd = "amixer -D pulse get Capture"
        ret = str(subprocess.check_output(cmd.split(' ')))
        return 'off' in ret

    def menu(self):
        menu = gtk.Menu()

        hotkey_configure_menu_item = gtk.MenuItem('Configure hotkey')
        hotkey_configure_menu_item.connect('activate', self.config_hotkey)
        menu.append(hotkey_configure_menu_item)

        toggle_menu_item = gtk.MenuItem(label='Toggle')
        toggle_menu_item.connect('activate', self.toggle)
        menu.append(toggle_menu_item)

        exit_menu_item = gtk.MenuItem(label='Quit')
        exit_menu_item.connect('activate', self.quit)
        menu.append(exit_menu_item)


        menu.show_all()
        return menu

    def config_hotkey(self, _):
        win = ConfigHotkeyWindow(self.hotkey)
        win.show_all()

    def set_mic_icon(self):
        self.indicator.set_icon_full(self.mic_icon,'muted')
        self.muted = False

    def set_mute_mic_icon(self):
        self.indicator.set_icon_full(self.muted_mic_icon,'unmuted')
        self.muted = True

    def toggle(self, _):
        cmd = "amixer -D pulse set Capture toggle"
        ret = str(subprocess.check_output(cmd.split(' ')))
        if 'off' in ret:
            self.set_mute_mic_icon()

        else:
            self.set_mic_icon()

    def toggle_icon(self):
        self.muted = not self.muted
        if self.muted:
            self.set_mute_mic_icon()
        else:
            self.set_mic_icon()

    def mic_state_thread(self):
        while not stop_threads:
            self.update_mic_state()
            time.sleep(0.3)

    def update_mic_state(self):
        muted = self.is_mic_muted()
        if muted != self.muted:
            self.toggle_icon()

    def quit(self, _):
        global stop_threads
        stop_threads = True
        self.thread.join()
        gtk.main_quit()

class KeyboardListener():
    def __init__(self, icon_tray, hotkey):
        self.icon_tray = icon_tray
        self.hotkey = hotkey

        # The currently active modifiers
        self.current = set()

    def _get_key_value(self, key):
        if hasattr(key, 'char') and key.char:
            key_value = ord(key.char)
        elif hasattr(key, 'value'):
            key_value = key.value.vk
        elif hasattr(key, 'vk'):
            key_value = key.vk
        return key_value

    def on_press(self, key):
        key_value = self._get_key_value(key)
        if any([key_value in COMBO for COMBO in self.hotkey.get_combinations()]):
            self.current.add(key_value)
            if any(all(k in self.current for k in COMBO) for COMBO in self.hotkey.get_combinations()):
                self.icon_tray.toggle(None)

    def on_release(self, key):
        self.current.clear()

    def listen_loop(self):
        listener = keyboard.Listener(on_press=self.on_press, on_release=self.on_release)
        listener.start()
        global stop_threads
        while not stop_threads:
            time.sleep(.3)

class Hotkey:
    def __init__(self, default=[{keyboard.Key.alt_l.value.vk, keyboard.Key.shift.value.vk ,ord('M')}]):
        self.config_filepath = 'mute_mic.conf'
        self.combination = default
        self.load_hotkey_from_file()

    def load_hotkey_from_file(self):
        try:
            with open(self.config_filepath) as file:
                config = yaml.load(file, Loader=yaml.FullLoader)
                if "hotkey" in config:
                    self.combination = [set(config['hotkey'])]
        except FileNotFoundError as error:
            with open(self.config_filepath,'w+') as file:
                config = {
                    'hotkey': list(self.combination[0])
                }
                yaml.dump(config, file)

    def save_new_combination(self, combination):
        self.combination = [set(combination)]
        config = {}
        with open(self.config_filepath,'r') as file:
            config = yaml.load(file, Loader=yaml.FullLoader)

        config['hotkey'] = combination
        with open(self.config_filepath,'w') as file:
            yaml.dump(config, file)

    def get_combinations(self):
        return self.combination

    def is_valid(self, candidate):
        mandatory_keys = ['Alt_L','Shift_L','Control_L','ISO_Level3_Shift','Control_R','Shift_R']
        if not candidate:
            return False
        elif not [key for key in candidate if Gdk.keyval_name(key) in mandatory_keys ]:
            return False
        return True


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    stop_threads = False

    hotkey = Hotkey()
    icon = SystemTrayIcon(hotkey)
    icon_thread = threading.Thread(target=icon.start)
    icon_thread.start()

    hotkey = KeyboardListener(icon, hotkey)
    hotkey_thread = threading.Thread(target=hotkey.listen_loop)
    hotkey_thread.start()

    while not stop_threads:
        time.sleep(.3)
    icon_thread.join()
    hotkey_thread.join()

