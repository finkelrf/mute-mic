# Mute Mic Icon Tray

<div style="text-align:center"><img src="https://drive.google.com/uc?export=download&id=1Bi5SrSisyPUZCe6JuvrZtpyNQTyySiXL" /></div>

An icon in the system tray indicating the state of microphone (Muted/Unmuted)
You can toggle the microphone mute state using the default hotkey "alt_l + shift_l + m" or change it using the **Configure Hotkey** item in the system tray menu.

## installation

``` bash
pip3 install pynput
pip3 install pyyaml
python3 mute_mic.py
```
